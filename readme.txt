== FONCTIONNEMENT ==

La solution consiste en un add-on javascript, nomm� " help.js ".

Le principe est simple : une fonction nomm�e isUserLost est appel�e � une fr�quence pr�cise (actuellement, toutes les 80ms),
elle se charge de v�rifier que l'activit� de l'utilisateur n'est pas � 0.
L'activit� est calcul�e par une variable globale, initialis�e � 100, et d�cr�ment�e toutes les 80ms par isUserLost.
Les �v�nements de clic et de d�placement de souris sont �cout�s afin d'augmenter la valeur d'activit�.
Il a �t� consid�r� qu'un clic repr�sentait une activit� plus forte qu'un simple d�placement de souris : ainsi, l'activit� est plus
faiblement augment�e en cas de d�placement de souris.

Enfin, dans le cas o� l'activit� atteint le seuil du 0, une infobox appara�t et demande � l'utilisateur si il a besoin d'aide.

Par la suite, une partie bien plus personnalisable (actuellement sp�cifique � notre site) permet de r�guler la mani�re pr�cise par laquelle l'aide se manifeste.
Par d�faut, sur notre site, nous mettons en opacit� tr�s faible (0.1) l'ensemble des �l�ments, en dehors d'une image sp�cifique, indiquant o� se trouve la page d'aide.

== INTEGRATION ==

Pour l'int�gration, il suffit d'int�grer le fichier help.js au site.

Seul le jeu d'opacit� sur les �l�ments, et l'affichage mettant en avant le clic sur la page d'aide doivent �tre supprim�s,
ces �l�ments �tant propres � notre site web.

Ainsi, les lignes 11 � 26, ainsi que les lignes 44 � 57 doivent �tre retir�es pour une utilisation tierce.

Elles peuvent �tre remplac�es par un comportement propre au site souhaitant implanter ce syst�me d'aide.