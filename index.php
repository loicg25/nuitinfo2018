<?php
ob_start('ob_gzhandler'); //démarre la bufferisation, compression du tampon si le client supporte gzip
session_start();

require_once 'php/bibli_generale.php';
require_once 'php/bibli_local.php';

error_reporting(E_ALL); // toutes les erreurs sont capturées (utile lors de la phase de développement)

html_debut("Equipe Stack Smashing", "css/style.css", ["plugins/help.js", "https://code.jquery.com/jquery-3.3.1.js", "plugins/bouttonTheme.js", "plugins/lamp.js"]);

html_nav('');

echo  '<main>
<header>
		Welcome to NAMIB !
		</header>
			<div id="intro">
				</p>Vous partez en exploration seul ? Dans un milieu aride ? Mais vous avez peur, vous ne savez pas comment gérer votre voyage sur place ?</p>
<h2>Ce site est fait pour vous !</h2>
<p>Grâce a lui vous aller pouvoir vous organiser, en répertoriant les tâches que vous avez à accomplir, tout en facilitant la gestion de votre base.</p>
<p> ce site vous renseignera sur les technologies innovantes et les nouveaux usages du monde connecté, grâce a son onglet Technologie. </p>
<p>Vous pourrez suivre l’évolution de votre santé facilement en cliquant sur l’onglet santé.</p>
			</div>
		  </main>';
		
html_footer();

html_lamp('');

html_fin();
?>
