-- phpMyAdmin SQL Dump
-- version 4.6.6deb5
-- https://www.phpmyadmin.net/
--
-- Client :  localhost:3306
-- Généré le :  Ven 07 Décembre 2018 à 07:19
-- Version du serveur :  5.7.24-0ubuntu0.18.04.1
-- Version de PHP :  7.2.10-0ubuntu0.18.04.1

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de données :  `stack_smashing`
--

-- --------------------------------------------------------

--
-- Structure de la table `date`
--

CREATE TABLE `date` (
  `id` int(11) NOT NULL,
  `date` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Contenu de la table `date`
--

INSERT INTO `date` (`id`, `date`) VALUES
(0, '2018-12-06 23:49:35'),
(1, '2018-12-06 23:48:39'),
(2, '2018-12-31 22:11:33'),
(3, '2018-12-06 23:50:38'),
(4, '2018-12-06 23:50:06');

-- --------------------------------------------------------

--
-- Structure de la table `devices`
--

CREATE TABLE `devices` (
  `id` int(11) NOT NULL,
  `name` text NOT NULL,
  `description` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Contenu de la table `devices`
--

INSERT INTO `devices` (`id`, `name`, `description`) VALUES
(0, 'Machine', 'Machine'),
(1, 'Other machine', 'This is another machine, described in more verbose terms');

-- --------------------------------------------------------

--
-- Structure de la table `deviceState`
--

CREATE TABLE `deviceState` (
  `device` int(11) NOT NULL,
  `date` int(11) NOT NULL,
  `state` enum('Excellent','Bon','Moyen','Endommagé','Non fonctionnel') NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Contenu de la table `deviceState`
--

INSERT INTO `deviceState` (`device`, `date`, `state`) VALUES
(0, 0, 'Bon'),
(0, 2, 'Excellent'),
(1, 0, 'Non fonctionnel');

-- --------------------------------------------------------

--
-- Structure de la table `health`
--

CREATE TABLE `health` (
  `date` int(11) NOT NULL,
  `heartRate` int(11) NOT NULL,
  `bloodPressure` float NOT NULL,
  `tidalVolume` float NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Contenu de la table `health`
--

INSERT INTO `health` (`date`, `heartRate`, `bloodPressure`, `tidalVolume`) VALUES
(3, 56, 55.55, 4568);

-- --------------------------------------------------------

--
-- Structure de la table `measurements`
--

CREATE TABLE `measurements` (
  `id` int(11) NOT NULL,
  `name` text NOT NULL,
  `description` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Contenu de la table `measurements`
--

INSERT INTO `measurements` (`id`, `name`, `description`) VALUES
(0, 'Vitesse du vent', 'Ceci est la vitesse du son');

-- --------------------------------------------------------

--
-- Structure de la table `measures`
--

CREATE TABLE `measures` (
  `measureType` int(11) NOT NULL,
  `date` int(11) NOT NULL,
  `value` float NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Contenu de la table `measures`
--

INSERT INTO `measures` (`measureType`, `date`, `value`) VALUES
(0, 4, 150.6);

-- --------------------------------------------------------

--
-- Structure de la table `tasks`
--

CREATE TABLE `tasks` (
  `id` int(11) NOT NULL,
  `name` text NOT NULL,
  `description` text,
  `done` tinyint(1) NOT NULL,
  `date` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Contenu de la table `tasks`
--

INSERT INTO `tasks` (`id`, `name`, `description`, `done`, `date`) VALUES
(0, 'Calculer le temps', 'Cette tâche consiste à calculer la vitesse du temps, je n\'ai malheureusement pas encore eu le temps de la réaliser.', 0, 4),
(1, 'S\'ennuyer', 'Une tâche qui n\'est que trop présente', 1, 3);

--
-- Index pour les tables exportées
--

--
-- Index pour la table `date`
--
ALTER TABLE `date`
  ADD PRIMARY KEY (`id`);

--
-- Index pour la table `devices`
--
ALTER TABLE `devices`
  ADD PRIMARY KEY (`id`);

--
-- Index pour la table `deviceState`
--
ALTER TABLE `deviceState`
  ADD PRIMARY KEY (`device`,`date`),
  ADD KEY `deviceState_date` (`date`);

--
-- Index pour la table `health`
--
ALTER TABLE `health`
  ADD PRIMARY KEY (`date`);

--
-- Index pour la table `measurements`
--
ALTER TABLE `measurements`
  ADD PRIMARY KEY (`id`);

--
-- Index pour la table `measures`
--
ALTER TABLE `measures`
  ADD PRIMARY KEY (`measureType`,`date`),
  ADD KEY `measures_date` (`date`);

--
-- Index pour la table `tasks`
--
ALTER TABLE `tasks`
  ADD PRIMARY KEY (`id`),
  ADD KEY `tasks_date` (`date`);

--
-- Contraintes pour les tables exportées
--

--
-- Contraintes pour la table `deviceState`
--
ALTER TABLE `deviceState`
  ADD CONSTRAINT `deviceState_date` FOREIGN KEY (`date`) REFERENCES `date` (`id`),
  ADD CONSTRAINT `deviceState_device` FOREIGN KEY (`device`) REFERENCES `devices` (`id`);

--
-- Contraintes pour la table `health`
--
ALTER TABLE `health`
  ADD CONSTRAINT `health_date` FOREIGN KEY (`date`) REFERENCES `date` (`id`);

--
-- Contraintes pour la table `measures`
--
ALTER TABLE `measures`
  ADD CONSTRAINT `measures_date` FOREIGN KEY (`date`) REFERENCES `date` (`id`),
  ADD CONSTRAINT `measures_measureType` FOREIGN KEY (`measureType`) REFERENCES `measurements` (`id`);

--
-- Contraintes pour la table `tasks`
--
ALTER TABLE `tasks`
  ADD CONSTRAINT `tasks_date` FOREIGN KEY (`date`) REFERENCES `date` (`id`);

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
