<?php 
ob_start('ob_gzhandler'); //démarre la bufferisation, compression du tampon si le client supporte gzip
session_start();

require_once 'bibli_generale.php';
require_once 'bibli_local.php';

error_reporting(E_ALL); // toutes les erreurs sont capturées (utile lors de la phase de développement)

html_debut("Aide", "../css/style.css", ["https://code.jquery.com/jquery-3.3.1.js", "../plugins/bouttonTheme.js", "../plugins/lamp.js"]);


html_nav('../');

echo '<header>
		Bonjour à vous grand-mère !	
		</header>
		<h2>Sur cette page vous apprendrez à utiliser le site !</h2>
		<p>Pour commencer, pour revenir à la page d’accueil vous pouvez <a href="../index.php"> cliquer ici </a>!<br>
		Sur la page d’accueil vous avez plusieurs onglets, 
		comme celui d’aide sur lequel vous venez de cliquer.<br>
		Nous allons détailler chaque onglet afin de vous faciliter la vie !</p>

<h3>Onglet santé : </h3>
<p> Ici vous pourrez entrer vos données afin de suivre votre état de santé au cours de votre aventure
Pour entrer vos données sur le site, il vous suffit de cliquer sur le champ de saisie, de les taper et d’appuyer sur le bouton valider. 
Suite à cela, un graphique ce créera, vous permettant de suivre l’évolution de votre état de santé au cours du temps.
<h3>Onglet tâche :</h3>
<p>L’onglet parfait si vous voulez faire un emploi du temps journalier ! 
Il vous permet d’ajouter des choses à accomplir dans votre journée, 
puis de les cocher lorsque vous les avez faites. Il se réinitialise à chaque fin de journée.</p>

<h3> Onglet technologie :</h3> 
<p>Cet onglet vous présente de nombreuses technologie que les chercheurs utilise.</p>
<h3>Onglet Gestion base :</h3>
<p>Cet onglet facilitera la gestion de votre base, pour vous préparer au mieux.  </p>';


html_footer();

html_lamp('../');

html_fin();
?>