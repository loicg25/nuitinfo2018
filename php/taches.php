<?php 
ob_start('ob_gzhandler'); //démarre la bufferisation, compression du tampon si le client supporte gzip
session_start();

require_once 'bibli_generale.php';
require_once 'bibli_local.php';

error_reporting(E_ALL); // toutes les erreurs sont capturées (utile lors de la phase de développement)

html_debut("Tâches", "../css/style.css", ["../plugins/help.js", "https://code.jquery.com/jquery-3.3.1.js", "../plugins/bouttonTheme.js", "../plugins/lamp.js"]);

html_nav('../');

$bd = bd_connect();

$sql = 	"SELECT * FROM `tasks` INNER JOIN `date` ON `date`.`id` = `tasks`.`date` ORDER BY 'date' DESC";

$res = mysqli_query($bd, $sql) or bd_erreur($bd,$sql);

print_tasks($res);

// libération des ressources
mysqli_free_result($res);
mysqli_close($bd);

html_footer();

html_lamp('../');

html_fin();

function print_tasks($arr) {
	$i = 0;
    while ($t = mysqli_fetch_assoc($arr)) {
        echo '<article>' .
                '<h3>' . $t['name'] . '</h3>' .
                '<label for="'. $i . '">' . $t['description'] . '</label>' .
                '<input name="' . $i . '" type="checkbox" disabled' .
                ($t['done'] ? 'checked' : '') . '>' . 
             '</article>';
		++$i;
    }
}
?>
