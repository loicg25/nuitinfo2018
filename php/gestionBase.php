<?php 
ob_start('ob_gzhandler'); //démarre la bufferisation, compression du tampon si le client supporte gzip
session_start();

require_once 'bibli_generale.php';
require_once 'bibli_local.php';

error_reporting(E_ALL); // toutes les erreurs sont capturées (utile lors de la phase de développement)

html_debut("Gestion de la base", "../css/style.css", ["../plugins/help.js", "https://code.jquery.com/jquery-3.3.1.js", "../plugins/bouttonTheme.js", "../plugins/lamp.js"]);

html_nav('../');

$bd = bd_connect();

$sql = 	"SELECT * FROM `measures` INNER JOIN `measurements` ON `measures`.`measureType` = `measurements`.`id` 
								INNER JOIN `date` ON `date`.`id` = `measures`.`date`";
								
$res = mysqli_query($bd, $sql) or bd_erreur($bd,$sql);

$t = mysqli_fetch_assoc($res);
var_dump($t);

// libération des ressources
mysqli_free_result($res);
mysqli_close($bd);

html_footer();

html_lamp('../');

html_fin();
?>