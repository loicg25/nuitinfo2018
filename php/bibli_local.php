<?php
/* Dans les commentaires qui suivront, je vais vous permettre de vous assurer
que dans la vie, il y as pire que de lire tous ces commentaires pour savoir si
nous avons réussi le défi "max commentaire drole" (ou quelque chose comme ça).

#VDM 1

Aujourd'hui, je suis professeur au collège et aucun de mes élèves ne se présente en cours.
Après avoir cherché un peu partout, je réalise que l’un d’eux a rajouté mon nom
sur la liste des professeurs absents. Personne n'avait rien remarqué. VDM

*/

/**
 *	Fonction affichant la barre de navigation et d'autres éléments utilitaires
 *
 *  @param 	String	$link Position relative des autres pages
 */
function html_nav($link) {
	echo '
		<img id="fleche" src="' . $link . 'ressources/fleche.png" alt="fleche" />
		<nav>
            <ul>
                <li><a href="' . $link . 'index.php">NAMIB</a></li>
                <li><a href="' . $link . 'php/sante.php">Santé</a></li>
                <li><a href="' . $link . 'php/technologies.php">Technologies</a></li>
                <li><a href="' . $link . 'php/taches.php">Tâches</a></li>
                <li><a href="' . $link . 'php/gestionBase.php">Gestion base</a></li>
				<li><a href="' . $link . 'php/help.php" id="help">Aide</a></li>
            </ul>
		</nav>

		<section id="sectionD">


		<div id="corps">
		';
}

/*

#VDM 2
Aujourd'hui, je surprends mon prof de maths en train de taguer les murs des
toilettes de la fac. Il a écrit : "Nique la fac". VDM

*/

/**
 *	Fonction affichant le pied de page.
 *
 */
function html_footer() {
	echo '	</div>
		</section>
		<footer>
			<a href="http://disc.univ-fcomte.fr/"><img src="http://sciences.univ-fcomte.fr/download/ufr-st/image/laboratoires/femto-st.jpg" alt="DISC" style="width: 150px"/></a>
			<a href="http://www.univ-fcomte.fr/"><img src="https://upload.wikimedia.org/wikipedia/fr/thumb/e/e2/Universit%C3%A9_de_Franche-Comt%C3%A9_%28logo%29.svg/619px-Universit%C3%A9_de_Franche-Comt%C3%A9_%28logo%29.svg.png" alt="UFC" style="width: 150px" /></a>
			<a href="http://www.jet1oeil.com/"><img src="http://www.jet1oeil.com/wp-content/uploads/2011/09/logo.png" alt="jet1oeil" /></a>
			<a href="http://www.smartesting.com/"><img src="http://www.smartesting.com/resources/images/smartesting/logo-smartesting-white.png" alt="smartesting"/></a>
			<a href="https://hiptest.net/"><img src="http://www.capitalgrandest.eu/wp-content/uploads/2016/01/hiptestLogo.jpg" alt="hiptest" style="background-color: black; width: 150px;" /></a>
			<a href="http://www.skinsoft.fr/"><img src="http://www.museumexperts.com/images/society/59fc4ad988169_logo%20skinsoft.jpg" alt="skinsoft" style="height:150px"></a>
			<a href="https://vixtechnology.com/"><img src="https://upload.wikimedia.org/wikipedia/en/4/4e/Vix_Technology_logo.png" alt="Vix Technology" style="height:150px"></a>
		</footer>';
}

/*

#VDM 3
Aujourd'hui, je suis considérée comme une grande déception par l'ensemble des
membres de ma famille. Mon crime ? Avoir été embauchée chez Auchan.
Eux travaillent tous chez Carrefour. VDM

*/

/**
 * Fonction affichant la lampe permettant de passer en thème nuit.
 * @param 	String	$link Position relative des autres pages
 */
function html_lamp($link) {
	echo '<input type="checkbox" alt="Modifier style" id="style">
		  <label id="ampoule" for="style"><img id="IMGampoule" src="', $link ,'ressources/ampouleEteinte.png" alt="Modifier le style"></label>

		  <div id="Theme">
			<ul id="ulD">
		        <li>
		            <span id = "jourD">Jour</span>
		            <span id = "nuitD">Nuit</span>
		        </li>
		    </ul>
		  </div>';
}


?>
