<?php 
ob_start('ob_gzhandler'); //démarre la bufferisation, compression du tampon si le client supporte gzip
session_start();

require_once 'bibli_generale.php';
require_once 'bibli_local.php';

error_reporting(E_ALL); // toutes les erreurs sont capturées (utile lors de la phase de développement)
ini_set('display_errors', 1);

html_debut("Technologies", "../css/style.css", ["../plugins/help.js", "https://code.jquery.com/jquery-3.3.1.js", "../plugins/bouttonTheme.js", "../plugins/lamp.js"]);

html_nav('../');

$bd = bd_connect();

$sql = 	"SELECT * FROM `deviceState` INNER JOIN `devices` ON `deviceState`.`device` = `devices`.`id`
								     INNER JOIN `date` ON `date`.`id` = `deviceState`.`date`";

$res = mysqli_query($bd, $sql) or bd_erreur($bd,$sql);

$t = process_device_states($res);

print_states($t);

// libération des ressources
mysqli_free_result($res);
mysqli_close($bd);

html_footer();

html_lamp('../');

html_fin();

function process_device_states($res) {
    $t = array();
    while ($r = mysqli_fetch_assoc($res)) {
        if (array_key_exists($r['device'], $t)) {
            $t[$r['device']]['dates'][$r['date']] = $r['state'];
        } else {
            $t[$r['device']]['name'] = $r['name'];
            $t[$r['device']]['description'] = $r['description'];
            $t[$r['device']]['dates'][$r['date']] = $r['state'];
        }
    }
    return $t;
}

function print_states($arr) {
    foreach ($arr as $device) {
        echo '<article><h3>' . $device['name'] . ':</h3>' .
             '<p>' . $device['description'] . '</p>' .
             '<ul>';
                foreach($device['dates'] as $date => $snapshot) {
                    echo '<li>' . $date . ' : ' . $snapshot . '</li>';
                }
        echo '</ul></article>';
    }
}

?>
