	var activity = 100; //Taux d'activité
	var mode_help = false; //Indique si l'aide est activé
	var old_help = document.getElementById("help");
	
	function isUserLost() {
		console.log(activity);
		--activity;
		if (activity <= 0 && !mode_help) {
			mode_help = true;
			
			document.querySelectorAll('nav > ul > li > a').forEach(function(el) { el.style.opacity = 0.1 });
						
			var help = document.getElementById("help");
			help.style.border = 'solid white 2px';
			help.style.fontSize = '32px';
			help.style.fontWeight = 'bold';
			help.style.opacity = 1;
			
			var sect = document.getElementById("sectionD");
			sect.style.opacity = 0.1;
			
			var footer = document.getElementsByTagName("footer")[0];
			footer.style.opacity = 0.1;
			
			var arrow = document.getElementById("fleche");
			arrow.style.display = "inline";
			
			alert("Avez-vous besoin d'aide ? Vous pouvez cliquer sur la page 'Aide' en haut à droite pour une assistance");

			activity = 100;
		}
	}
	
	function printActivity() {
		console.log(activity);
	}
	
	document.addEventListener("click", function() {
		activity = 100; //Activité maximale
		
		if (mode_help) {
			mode_help = false;
			
			document.querySelectorAll('nav > ul > li > a').forEach(function(el) { el.style.opacity = 1 });
			var help = document.getElementById("help");
			help.style.border = 'none';
			help.style.fontSize = '18px';
			help.style.fontWeight = 'normal';
			
			var sect = document.getElementById("sectionD");
			sect.style.opacity = 1;
			
			var footer = document.getElementsByTagName("footer")[0];
			footer.style.opacity = 1;
			
			var arrow = document.getElementById("fleche");
			arrow.style.display = "none";
		}
	});
	
	document.addEventListener("mousemove", function() {
		if (activity < 100) {
			activity += 0.2; //Faible activité
		}
	});
	
	var lost = setInterval(isUserLost, 80);
