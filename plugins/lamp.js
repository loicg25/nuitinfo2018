/*Petite lampe pour mettre le menu permettant d'allumer la lumière mais aussi de choisir pleins de thèmes!!! Enfin si on avait réussi à les créer... :'( */

window.onload = function() {
			var img_ampoule = document.getElementById("IMGampoule").src;
			document.getElementById("style").addEventListener("change", function(){

				if(this.checked){
					document.getElementById("IMGampoule").src = img_ampoule.replace("ampouleEteinte", "ampouleAllumee");
				}else{
					document.getElementById("IMGampoule").src = img_ampoule.replace("ampouleAllumee", "ampouleEteinte");
				}
			});
}
