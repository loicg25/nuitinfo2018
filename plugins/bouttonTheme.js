/* Bouton permettant de dire "il va faire tout noire!" (mode nuit) sur le site */

$(document).ready(function() {
    $('#ulD').click(function() {
        $('#ulD').toggleClass('active')
        $('#sectionD').toggleClass('dark')
        $('footer').toggleClass('dark')
    })
})
